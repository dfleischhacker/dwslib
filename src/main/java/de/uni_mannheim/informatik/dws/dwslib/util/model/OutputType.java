package de.uni_mannheim.informatik.dws.dwslib.util.model;

public enum OutputType {
	
	PLAIN,
	GZIP;

}
